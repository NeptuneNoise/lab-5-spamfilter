
import com.noise.SpamFilter;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.Assert;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class Testl5 {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @BeforeClass
    public static void start() {
        System.out.println("Запуск тестов...");
    }
    @Test
    public void FileTest(){
        Assert.assertTrue(new File("mbox.txt").exists());
        Assert.assertTrue(new File("spamdata.txt").exists());
        Assert.assertTrue(new File("spam.txt").exists());
    }
    @Test
    public void FileNotFoundExceptionTest(){
        Exception exception = assertThrows(Exception.class, () -> SpamFilter.Sorter(new File("not_exist_file.txt")));
        Assert.assertTrue(exception instanceof FileNotFoundException);
    }
    @Test
    public void FileNotFoundExceptionTest2(){
        Exception exception = assertThrows(Exception.class, () -> SpamFilter.Sorter(new File("mbox.txt")));
        Assert.assertFalse(exception instanceof FileNotFoundException);
    }
    @Test
    public void NullPointerTest(){
        Exception exception = assertThrows(Exception.class, () -> SpamFilter.Sorter(new File("emptyfile.txt")));
        Assert.assertTrue(exception instanceof NullPointerException);
    }
    @Test
    public void getAuthorTest() {
        ArrayList<String> author=new ArrayList<>();
        author.add("mail@test.edu");
        author.add("mail@test2.edu");
        assertEquals(2, SpamFilter.getAuthor(author).size());
    }
    @Test
    public void getSpamTest() {
        ArrayList<String> spam=new ArrayList<>();
        spam.add("0.5486");
        spam.add("0.2596");
        spam.add("0.7834");
        spam.add("0.9143");
        spam.add("0.0281");
        assertEquals(5, SpamFilter.getSpam(spam).size());
    }
    @Test
    public void testNullException() {
        thrown.expect(NullPointerException.class);
        SpamFilter.getSpam(null);
        SpamFilter.getAuthor(null);
    }
    @Test
    public void AuthorTest(){
        Assert.assertTrue(SpamFilter.getAuthor(new ArrayList<>()).isEmpty());
        Assert.assertFalse(SpamFilter.getAuthor(new ArrayList<>(Collections.singleton("mail@test.edu"))).isEmpty());
    }
    @Test
    public void SpamTest(){
        Assert.assertTrue(SpamFilter.getSpam(new ArrayList<>()).isEmpty());
        Assert.assertFalse(SpamFilter.getSpam(new ArrayList<>(Collections.singleton("0.5486"))).isEmpty());
    }
    @Test
    public void testFileWriting() throws IOException {
        PrintWriter outputFile = new PrintWriter(
                new FileWriter("spamdata.txt", true));
        outputFile.append("mail@test.edu ");
        outputFile.append("X-DSPAM-Confidence: 0.7619");
        outputFile.flush();
        outputFile.close();
    }

    @AfterClass
    public static void finish() {
        System.out.println("Тесты Завершенны...");
    }
}
