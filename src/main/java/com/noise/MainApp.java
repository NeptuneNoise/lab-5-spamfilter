package com.noise;

import java.io.FileOutputStream;
import java.io.IOException;

public class MainApp {
    public static void main(String[] args) throws IOException {
        new FileOutputStream("smadata.txt", false).close();
        new LineChartMain().start();
    }
}
