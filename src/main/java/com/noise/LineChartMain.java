package com.noise;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;


public class LineChartMain extends Application {

    @Override public void start(Stage stage) throws IOException {
        double index = 0;
        double [] spamdata = new double[0];
        System.out.println("Диаграмма по отправителям");
        try{
            do {
                Path path = Paths.get("spam.txt");
                spamdata = Files.lines(path)
                        .flatMap(e -> Stream.of(e.split(" ")))
                        .mapToDouble(Double::parseDouble)
                        .toArray();
                System.out.print(spamdata[(int) index++] + " ");
            } while (index < spamdata.length);

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        double[] xArray2 = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46};

        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("");
        yAxis.setLabel("");
        final javafx.scene.chart.LineChart<Number,Number> lineChart = new javafx.scene.chart.LineChart<Number,Number>(xAxis,yAxis);
        XYChart.Series series2 = new XYChart.Series();
        series2.setName("Значение параметра X-DSPAM-Confidence:");
        for(int i = 0; i<xArray2.length; i++) {
            series2.getData().add(new XYChart.Data(xArray2[i], spamdata[i]));
        }
        Scene scene  = new Scene(lineChart,800,600);
        lineChart.getData().add(series2);
        series2.getNode().setStyle("-fx-stroke: #6263ff;");
        stage.setScene(scene);
        stage.show();
    }
    //-fx-fill: #c1ffba;

    public static void main(String[] args) {
        launch(args);

    }

    public void start() {
        launch();
    }
}