package com.noise;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;



public class SpamFilter {
    public SpamFilter() {
    }
    public static void Sorter(File file1) throws IOException {
        StringBuilder sb = new StringBuilder();
        StringBuilder sp = new StringBuilder();
        System.currentTimeMillis();
        System.currentTimeMillis();
        String[] sp_temp;

        File file = new File("mbox.txt");
        FileReader fr = new FileReader(file);
        FileReader fr1 = new FileReader(file1);
        BufferedReader reader = new BufferedReader(fr);
        String line = reader.readLine();
        while (line != null) {

            line = reader.readLine();
            List<String> allMatches = new ArrayList<>();

            Pattern pattern = Pattern.compile("Authоr: \\w.+");
            Matcher matcher = pattern.matcher(line);
            while (matcher.find()) {
                allMatches.add(matcher.group());
                String[] a_temp = allMatches.toArray(new String[0]);
                String[] author_temp = new String[a_temp.length];
                for (int i = 0; i < a_temp.length; i++) {
                    author_temp[i] = a_temp[i].substring(a_temp[i].lastIndexOf("Author: ") + 8);
                    for (String s : author_temp) {
                        sb.append(s);
                        sb.append("\t");
                    }
                }
            }
            Pattern pattern2 = Pattern.compile("X-DSPAM-Confidence: \\w.+");
            Matcher matcher2 = pattern2.matcher(line);
            while (matcher2.find()) {
                allMatches.add(matcher2.group());
                String[] s_temp = allMatches.toArray(new String[0]);
                sp_temp = new String[s_temp.length];
                for (int i = 0; i < s_temp.length; i++) {
                    sp_temp[i] = s_temp[i].substring(s_temp[i].lastIndexOf("X-DSPAM-Confidence: ") + 20);
                    PrintWriter outputFile = new PrintWriter(
                            new FileWriter("spamdata.txt", true));
                    try {
                        for (String s : sp_temp) {
                            outputFile.println(s);
                        }
                    } finally {
                        outputFile.flush();
                        outputFile.close();
                    }
                    for (String s : sp_temp) {
                        sp.append(s);
                        sp.append("\n");
                    }
                }
            }
        }
    }

    public static ArrayList<String> getAuthor(ArrayList<String> author){
        for (int i=0; i<author.size(); i++)
        {
            int g=i+1;
            while (g<author.size())
            {
                if (author.get(i).equals(author.get(g)))
                {
                    author.remove(g);
                    g-=1;
                }
                g++;
            }
        }
        return author;
    }
    public static ArrayList<String> getSpam(ArrayList<String> spam){
        for (int i=0; i<spam.size(); i++)
        {
            int g=i+1;
            while (g<spam.size())
            {
                if (spam.get(i).equals(spam.get(g)))
                {
                    spam.remove(g);
                    g-=1;
                }
                g++;
            }
        }
        return spam;
    }
    public static void main(String[] args){
        try(BufferedReader br = new BufferedReader(new FileReader("mbox.txt")))
        {
            String s;
            Pattern pattern = Pattern.compile("\\w+([.-]?\\w+)*@\\w+([\\-.]?\\w+)*\\.\\w{2,4}");
            Matcher matcher;
            ArrayList<String> author = new ArrayList<>();
            while((s=br.readLine())!=null){
                if (s.contains("From:"))
                {
                    matcher = pattern.matcher(s);
                    if (matcher.find()) {
                        author.add(matcher.group());
                    }
                }
            }
            ArrayList<String> spam = new ArrayList<>();
            while((s=br.readLine())!=null){
                if (s.contains("X-DSPAM-Confidence:"))
                {
                    matcher = pattern.matcher(s);
                    if (matcher.find()) {
                        spam.add(matcher.group());
                    }
                }
            }
            br.close();
            author.toArray(new String[0]);
            spam.toArray(new String[0]);
            ArrayList<String> author_temp=getAuthor(author);
            System.out.println("Колличество авторов: " + author_temp.size());
            int j=0;
            ArrayList <Float> cf = new ArrayList<>();
            HashMap<String, Float> cfr = new HashMap<>();
            HashMap<String, Integer> mail = new HashMap<>();
            String s1;
            String[] f;
            int mail_c=0;
            while (j<author_temp.size())
            {
                BufferedReader br1 = new BufferedReader(new FileReader("mbox.txt"));

                while((s=br1.readLine())!=null)
                {
                    if(s.equals("From: "+author_temp.get(j))) {
                        while ((s1 = br1.readLine())!=null) {
                            if (s1.contains("X-DSPAM-Confidence:")) {
                                f = s1.split(" ");
                                cf.add(Float.parseFloat(f[1]));
                                mail_c++;
                                break;
                            }
                        }
                    }
                }
                if(mail_c!=0)
                {
                    float y = 0;
                    for (Float aFloat : cf) {
                        y += aFloat;
                    }
                    float c = y / cf.size();
                    cfr.put(author_temp.get(j), c);
                    mail.put(author_temp.get(j), mail_c);
                    mail_c = 0;
                }
                j+=1;
                br1.close();
                cf.clear();
            }
            System.out.println("Коэффициент X-DSPAM:");
            System.out.println(cfr);
            System.out.println("Забаненные письма:");
            System.out.println(mail);
        }

        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }
}










